// Empty write any javascript that you need for the product page

let mainImage = document.querySelector('.product-img img');
let mainImageSrc = mainImage.getAttribute('src');
let thumbnails = document.querySelectorAll('.thumbnails img');

console.log(mainImageSrc);

for (var i = 0; i < thumbnails.length; i++) {
    thumbnails[i].addEventListener('mouseover', function () {
        const link = this.getAttribute("src");
        mainImage.setAttribute('src', link);
    });
}


